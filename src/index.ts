import express, { Application, Request, Response } from 'express';
import cron from 'node-cron';
import bodyParser from 'body-parser';
import routes from './routes';
import connect from './utils/connect';
import { scrapers } from './scrapers';

const app: Application = express();
const port = 5000;
const db = 'mongodb://localhost:27017/bitscraper';

// 5 seconds: '*/5 * * * * *', 1 minute: '* * * * *'
cron.schedule('* * * * *', () => {
  scrapers.forEach(scraper => scraper());
});

app.use(bodyParser.json());
app.listen(port, () => console.log(`Server running on port ${port}`));
connect({db});
routes({ app });
