import mongoose, { Schema } from 'mongoose';
import { IDelistings } from '../utils';

const basicProps = { type: String, required: true };
const DelistingsSchema: Schema = new Schema({
  exchange: basicProps,
  pairs: basicProps,
  date: basicProps
});

export default mongoose.model<IDelistings>('Delistings', DelistingsSchema, 'delistings');
