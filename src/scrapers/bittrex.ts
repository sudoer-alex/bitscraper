import axios from 'axios';
import cheerio from 'cheerio';
import moment from 'moment';
import { AddDelistingRecord, GetDelistings } from '../controllers/delistings';
import {
  BITTREX,
  IAnnouncement,
  IDelisting,
  IDelistings,
  isFutureDate
} from '../utils';

const baseUrl = 'https://bittrex.zendesk.com';
const pageUrl = `${baseUrl}/hc/en-us/sections/200560334-Coin-Removals`;
const delistingSeparator = 'Pending Market Removals ';
const tradingPairSeparator = ', ';
const announcementsSelector = 'ul.article-list > li > a';
const articleDetailsSelector = 'article .article__body p';

export const bittrex = async () => {
  const historyItems: IDelisting[] = await getDelistingsHistory();
  const announcements: IAnnouncement[] = await getActiveAnnouncements();

  announcements.forEach(async announcement => {
    const { url, date } = announcement;
    const pairs: string[] = await getDelistingPairs(url);

    if (pairs && pairs.length) {
      const result = {
        exchange: BITTREX.label,
        pairs: pairs.join(tradingPairSeparator),
        date: moment(date).toISOString()
      };

      if (historyItems.findIndex(item =>
          item.exchange === result.exchange &&
          item.date === result.date) === -1
      ) {
        addDelistingInfo(result);
      }
    }
  });
};

const getDelistingsHistory = async (): Promise<IDelistings[]> => {
  return GetDelistings({
    exchange: BITTREX.label,
    date: { $gte: moment().toISOString() }
  });
};

const getActiveAnnouncements = async (): Promise<IAnnouncement[]> => {
  try {
    const { data: html } = await axios.get(pageUrl);
    const $ = cheerio.load(html);
    const announcements: IAnnouncement[] = [];

    $(announcementsSelector).each((i, item) => {
      if ($(item).text().includes(delistingSeparator)) {
        const dateStr = $(item).text().split(delistingSeparator).pop();

        if (dateStr && dateStr.length) {
          const date: Date = moment(dateStr, BITTREX.dateFormat).toDate();

          if (isFutureDate(date)) {
            const announcementLink = $(item).attr('href');
            let url;

            if (announcementLink.includes('https://') ||
              announcementLink.includes('http://')) {
              url = announcementLink;
            } else {
              url = baseUrl + announcementLink;
            }

            announcements.push({ url, date });
          }
        }
      }
    });

    return announcements;
  } catch (error) {
    console.log(error);

    return [];
  }
};

const getDelistingPairs = async (url: string): Promise<string[]> => {
  try {
    const { data: html } = await axios.get(url);
    const $ = cheerio.load(html);
    const delistingPairs: string[] = [];

    $(articleDetailsSelector).each((i, item) => {
      const text = $(item).text().trim();

      if (BITTREX.tradingPairRegex.test(text)) {
        delistingPairs.push(text);
      }
    });

    return delistingPairs;
  } catch (error) {
    console.log(error);

    return [];
  }
};

const addDelistingInfo = async (payload: IDelisting) => {
  // TODO emit new values to rocket chat
  return AddDelistingRecord(payload);
};
