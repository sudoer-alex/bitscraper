import Delistings from '../models/delistings';
import { IDelisting, IDelistings } from '../utils';

export const GetDelistings =
  async (params: any): Promise<IDelistings[]> => {
    return Delistings.find(params);
  };

export const AddDelistingRecord =
  async ({ exchange, pairs, date }: IDelisting): Promise<IDelistings> => {
    return Delistings.create({ exchange, pairs, date });
  };
