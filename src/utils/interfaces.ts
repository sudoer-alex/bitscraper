import { Document } from 'mongoose';

export interface IAnnouncement {
  url: string,
  date: Date
}

export interface IDelisting {
  exchange: string,
  pairs: string,
  date: string
}

export interface IDelistings extends IDelisting, Document {}
