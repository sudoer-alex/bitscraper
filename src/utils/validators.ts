import moment from 'moment';
import { DEFAULT_DATE_FORMAT, EXCHANGES } from './constants';

export const isExistingExchange = (value: string) => {
  return EXCHANGES.findIndex(exchange => exchange.label === value) !== -1;
};

export const isValidDateString = (value: string) => {
  return moment(value, [moment.ISO_8601, DEFAULT_DATE_FORMAT], true).isValid();
};
