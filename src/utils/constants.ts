export const DEFAULT_DATE_FORMAT = 'MM/DD/YYYY';

export const BITTREX = {
  label: 'Bittrex',
  dateFormat: DEFAULT_DATE_FORMAT,
  tradingPairRegex: /^[a-z0-9]{3,5}-[a-z0-9]{3,5}$/i
};

export const BINANCE = {
  label: 'Bittrex',
  dateFormat: DEFAULT_DATE_FORMAT
};

export const EXCHANGES = [BITTREX, BINANCE];
