import moment from 'moment';
import { DEFAULT_DATE_FORMAT } from './constants';

export const isFutureDate = (date: Date) => {
  return date.setHours(0,0,0,0) > new Date().setHours(0,0,0,0);
};

export const toISOString = (date: any) => {
  return moment(date, DEFAULT_DATE_FORMAT).toDate().toISOString();
};
