import { AddDelistingRecord, GetDelistings } from '../controllers/delistings';
import { toISOString, isValidDateString } from '../utils';
import { Application, Request, Response } from 'express';

type TRoutesInput = {
  app: Application;
};

const buildMongoQuery = (query: any) => {
  const { exchange, date } = query;
  const result: any = {};

  if (exchange) {
    // TODO sanitize input value
    result.exchange = exchange;
  }

  if (date && isValidDateString(date)) {
    result.date = { $gte: toISOString(date) };
  }

  return result;
};

export default ({ app }: TRoutesInput) => {
  app.get('/api/feed', async (req: Request, res: Response) => {
    const query = buildMongoQuery(req.query);

    GetDelistings(query)
      .then(data => {
        res.status(200).json(data);
      })
      .catch(err => {
        console.log('err', err);
        res.status(500).json({ error: err });
      });
  });

  // app.post('/api/delisting', async (req, res) => {
  //   // TODO sanitize user input
  //   const delisting = await AddDelistingRecord({
  //     exchange: req.body.exchange,
  //     pairs: req.body.pairs,
  //     date: req.body.date
  //   });
  //   return res.json({ delisting });
  // });
}
