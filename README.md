# Bitscraper

Bitscraper is a Typescript library for aggregating delisting announcements.

## Installation

Install dependencies
```bash
npm install
```

Connect to your remote `mongo` instance by changing `db` config value.

(Optional) For full local instance install `mongodb` and create database
```
use bitscraper
db.createCollection('delistings')
```

## Usage

Development mode
```bash
npm run dev
```

Production mode
```bash
npm run build
npm run start
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[ISC](https://choosealicense.com/licenses/isc/)